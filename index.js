/**
 * @format
 */
import React from 'react';
import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import { Provider } from "react-redux";
import CreateStore from "./src/store/index";
const WrappedApp = ()=>{
    const store = CreateStore();
    return (
        <Provider store={store}>
        <App></App>
        </Provider>
    )
}
AppRegistry.registerComponent(appName, () => WrappedApp);
