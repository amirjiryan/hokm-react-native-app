/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useState , useEffect } from "react";
import { StyleSheet, Text, View, ImageBackground } from "react-native";
import Socket from "./src/socket/index.js";
import { useSelector } from 'react-redux'
import { Colors } from "react-native/Libraries/NewAppScreen";
import NameInput from './src/components/room/nameInput'
import Rooms from './src/pages/rooms'
import Game from './src/pages/game'
import useSocketListener from './src/custom-hooks/useSocketListener'
import Cards from './src/components/cards/cards'
const App = () => {
  const game = useSelector(state => state.game);
  const name = useSelector(state => state.name);
  const [error, setError] = useState("");
  Socket.on("connect", function () {
    setError("connected");
  });

useSocketListener();
  return (

    <ImageBackground
      source={require("./src/img/background.jpg")}
      style={styles.image}
    >
      <View style={styles.container}>
        <Text>{error}</Text>
  
      {!name ? < NameInput /> : game ? <Game gameName={game} name={name} /> : <Rooms />}

      </View>
    </ImageBackground>

  );
};

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  engine: {
    position: "absolute",
    right: 0,
  },
  body: {
    backgroundColor: Colors.white,
  },
  image: {
    flex: 1,
    resizeMode: "cover",
    justifyContent: "center",
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: "600",
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: "400",
    color: Colors.dark,
  },
  highlight: {
    fontWeight: "700",
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: "600",
    padding: 4,
    paddingRight: 12,
    textAlign: "right",
  },
});

export default App;
