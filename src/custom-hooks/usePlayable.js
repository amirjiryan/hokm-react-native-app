import { useState } from "react";
import Socket from "../socket/index";
import { useSelector } from "react-redux";
function usePlayable(event, data) {
  const game = useSelector((state) => state.game);
  const name = useSelector((state) => state.name);
  const handleDrag = (pan) => {
    var dragged = pan.__getValue();
    if (dragged < -110) {
      Socket.emit(event, ...data, name, game, function (err) {
        if (err) {
          pan.setValue(0);
        }
      });
    } else {
      pan.setValue(0);
    }
  };
  return [handleDrag];
}
export default usePlayable;
