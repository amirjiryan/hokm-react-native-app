import { useState } from 'react'
function useInput(name) {
    const [input, setInput] = useState(name);
    const handleChange = (input) => setInput(input);
    return [input, handleChange]
}
export default useInput