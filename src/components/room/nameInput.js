import React, { useEffect, useState } from 'react';
import useInput from '../../custom-hooks/useInput';
import { Text, View, TextInput, Button } from 'react-native';
import { useDispatch} from 'react-redux'
import Socket from '../../socket/index'
import addName from '../../store/actions/name'
const NameInput = ({ name, setName }) => {
    const [input, handleChange] = useInput(name);
    const [error, setError] = useState(false);
    const dispatch = useDispatch();
    function sendName() {
        Socket.emit("sendName", input, function (err) {
            if (err) {
                return setError(true);
            }
            console.log("here")
            // bug : calling set name here when name is present in local state cause it to dispath twice
            dispatch(addName(input));
        });
    }
    useEffect(() => {
        if (input) { sendName() }
    }, []);
    return (
        <View>
            {!error ? null : <Text style={{ backgroundColor: "red" }}>"این نام وجود دارد . لطفا یک نام دیگر وارد کنید"</Text>}
            <TextInput
                style={{ height: 40, borderColor: 'gray', borderWidth: 1, backgroundColor: "white", marginBottom: 30 }}
                onChangeText={text => handleChange(text)}
                value={input}
                placeholder="لطفا نام خود را وارد کنید "
            />
            <Button
                onPress={() => sendName()}
                title="ورود"
                color="#841584"
                accessibilityLabel="دکمه ورود بازی"
            />
        </View>
    );
}

export default NameInput;