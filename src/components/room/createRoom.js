import React from 'react';
import useInput from '../../custom-hooks/useInput'
import Socket from '../../socket/index'
import { View, Button, TextInput } from 'react-native'
const CreateRoom = () => {
    const [input, handleChange] = useInput("");
    const handleSubmit = () => { Socket.emit("create-room", input); };
    return (

        <View>
            <TextInput
                style={{ height: 40, borderColor: 'gray', borderWidth: 1 }}
                onChangeText={text => handleChange(text)}
                value={input}
            />
            <Button
                onPress={handleSubmit}
                title="Learn More"
                color="#841584"
                accessibilityLabel="Learn more about this purple button"
            />
        </View>
        // <form onSubmit={handleSubmit}>
        //     <input className="input field" name="roomInput" onChange={handleChange} />
        //     <input className="input button" type="submit" value="افزودن اتاق جدید" />
        // </form>

    );
}

export default CreateRoom;