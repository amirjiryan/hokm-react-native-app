import React from 'react';
import { useDispatch } from "react-redux"
import addGame from '../../store/actions/game';
import { Button } from 'react-native'
import addName from '../../store/actions/name';
const Room = ({ room }) => {
    const dispatch = useDispatch();
    return (
        <Button
            onPress={() => dispatch(addGame(room))}
            title={room}
            color="#841584"
            accessibilityLabel="نام اتاق"
        />



    );
}

export default Room;