import React from 'react';
import Playablesuit from './playableSuit';
import { FlatList, StyleSheet } from 'react-native'
const Hokms = ({ suits }) => {

    return (
        <FlatList
            horizontal={true}
            data={suits}
            renderItem={({ item }) => <Playablesuit suit={item} />}
        />
    )

}

export default Hokms;