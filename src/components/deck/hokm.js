import React from 'react';
import { useSelector } from 'react-redux';
import Hokms from './hokms';
import { Text, View, StyleSheet } from 'react-native'

const Hokm = () => {
    const hakem = useSelector(state => state.hakem);
    const hokm = useSelector(state => state.hokm);
    const name = useSelector(state => state.name);
    return (
        <View style={styles.hokm}>
            {(hakem === name && !hokm) ? <Hokms suits={["del", "khaj", "khesht", "pik"]} /> : null}
            <Text>{(!hokm && hakem !== name) ? "منتظر حکم بمانید" : null}</Text>
        </View>
    );
}
const styles = StyleSheet.create({
    hokm: {
        marginTop:-100,
        top: "50%",
        zIndex: 11111111111
    }
})
export default Hokm;