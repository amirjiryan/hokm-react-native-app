import React from 'react';
import Player from "./player";
import { useSelector } from 'react-redux';
import { View, FlatList, StyleSheet, Text } from 'react-native'
const Players = () => {
    const players = useSelector(state => state.players);
    const name = useSelector(state => state.name)

    return (
        <View style={styles.players}>
            {players.map(player => player !== name ? <Player name={player}></Player> : null)}
        </View>
    )
}
const styles = StyleSheet.create({
    players: {
        flex: 1,
        justifyContent: "center",
        overflow: "visible",
        width: "100%",
        height: "100%",
        position: "absolute"


    }
})
export default Players;