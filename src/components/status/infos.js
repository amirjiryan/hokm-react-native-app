import React, { useEffect, useState } from 'react';
import Socket from "../../socket/index";
import Suit from '../deck/suit';
import { useSelector } from 'react-redux';
import { View, Text, SectionList, FlatList, StyleSheet, TouchableWithoutFeedback } from 'react-native'
const Infos = ({ game }) => {
    const [hokm, setHokm] = useState('del');
    const [hakem, setHakem] = useState();
    const teams = useSelector(state => state.teams);
    useEffect(() => {
        Socket.on("taeen-hakem", function (name) {
            setHakem(name);
        })
        Socket.on("hokm", function (hokm) {
            setHokm(hokm);
        });
    }, []);
    return (
        <View style={styles.infos}>
            <Text>{game}: نام اتاق </Text>
            <FlatList
                data={teams}
                renderItem={({ item, index }) => <Text>{"تیم" + (index+1) + ' : ' + item.won_bazi + "-" + item.won_dast}</Text>}
            />
            <Text>{hokm ? "حکم : " + hokm : "حکم تعیین نشده"}</Text>
        </View>
    );
}
const styles = StyleSheet.create({
    infos: {
        position: "absolute",
        top: 0,
        right: 0,
        overflow: "scroll",
        backgroundColor: "green"
    }
})
export default Infos;