import React from "react";
import Card from './card';
import usePlayable from '../../custom-hooks/usePlayable';
import Draggable from '../deck/Draggable'
const PlayableCard = ({ card }) => {
  const [handleDrag] = usePlayable("sendcard", card);
  return (
    <Draggable
      panRelease={handleDrag}
    >
      <Card card={card} style={{
        paddingTop: 200,
        overflow: "hidden"
      }}></Card>
    </Draggable>
  );
}
// controlled component , composable component , socket.emit callbackfunction
export default PlayableCard;