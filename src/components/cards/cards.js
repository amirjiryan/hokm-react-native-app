import React, { useState, useEffect } from "react";
import Playable from "./playableCard";
import sortCards from "../../logic/sortCards";
import Socket from "../../socket/index";
import { View, FlatList, StyleSheet } from "react-native";
const Cards = () => {
  const [cards, setCards] = useState([ ]);
  const [showall, setShowall] = useState(true);
  const [turn, setTurn] = useState(false);
  useEffect(() => {
    Socket.on("cards", function (cards) {
      setCards(cards);
    });
    Socket.on("your_turn", function (turn) {
      setTurn(turn);
    });
    Socket.on("taeen-hakem", function (name) {
      setShowall(false);
      console.log("here");
    });
    Socket.on("hokm", function () {
      setCards((prevstate) => [...sortCards(prevstate, true)]);
      setShowall(true);
    });
    Socket.on("remove-card", function (card) {
      console.log(card);
      setCards((prevCards) =>
        prevCards.filter(
          (prevcard) => prevcard[0] !== card[0] || prevcard[1] !== card[1]
        )
      );
    });
  }, []);
  return (
    <View style={styles.bottom}>
      {showall ?
        <FlatList
          horizontal={true}
          style={styles.cards}
          data={cards}
          keyExtractor={(item, index) => "" + item}
          renderItem={({ item }) => <Playable card={item} key={"" + item} />}
        />
        :
        <FlatList
          horizontal={true}
          style={styles.cards}
          data={cards.slice(0, 5)}
          keyExtractor={(item, index) => "" + item}
          renderItem={({ item }) => <Playable card={item} key={"" + item} />}

        />}
    </View>
  );
};
const styles = StyleSheet.create({
  cards: {
    paddingTop: 400,
    zIndex: 10000,
    overflow: "visible",
  },
  bottom: {
    flex: 1,
    justifyContent: "center",
    position: "absolute",
    bottom: 0,
    overflow: "scroll",
    paddingTop: 400
  },
});
export default Cards;
