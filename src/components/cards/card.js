import React from 'react';
import { Suits } from '../../types/suits';
import { Image, StyleSheet, View } from 'react-native'
const images = {
    _1del: require("./../../img/1del.png"),
    _2del: require("./../../img/2del.png"),
    _3del: require("./../../img/3del.png"),
    _4del: require("./../../img/4del.png"),
    _5del: require("./../../img/5del.png"),
    _6del: require("./../../img/6del.png"),
    _7del: require("./../../img/7del.png"),
    _8del: require("./../../img/8del.png"),
    _9del: require("./../../img/9del.png"),
    _10del: require("./../../img/10del.png"),
    _11del: require("./../../img/11del.png"),
    _12del: require("./../../img/12del.png"),
    _13del: require("./../../img/13del.png"),
    _1pik: require("./../../img/1pik.png"),
    _2pik: require("./../../img/2pik.png"),
    _3pik: require("./../../img/3pik.png"),
    _4pik: require("./../../img/4pik.png"),
    _5pik: require("./../../img/5pik.png"),
    _6pik: require("./../../img/6pik.png"),
    _7pik: require("./../../img/7pik.png"),
    _8pik: require("./../../img/8pik.png"),
    _9pik: require("./../../img/9pik.png"),
    _10pik: require("./../../img/10pik.png"),
    _11pik: require("./../../img/11pik.png"),
    _12pik: require("./../../img/12pik.png"),
    _13pik: require("./../../img/13pik.png"),
    _1khaj: require("./../../img/1khaj.png"),
    _2khaj: require("./../../img/2khaj.png"),
    _3khaj: require("./../../img/3khaj.png"),
    _4khaj: require("./../../img/4khaj.png"),
    _5khaj: require("./../../img/5khaj.png"),
    _6khaj: require("./../../img/6khaj.png"),
    _7khaj: require("./../../img/7khaj.png"),
    _8khaj: require("./../../img/8khaj.png"),
    _9khaj: require("./../../img/9khaj.png"),
    _10khaj: require("./../../img/10khaj.png"),
    _11khaj: require("./../../img/11khaj.png"),
    _12khaj: require("./../../img/12khaj.png"),
    _13khaj: require("./../../img/13khaj.png"),
    _1khesht: require("./../../img/1khesht.png"),
    _2khesht: require("./../../img/2khesht.png"),
    _3khesht: require("./../../img/3khesht.png"),
    _4khesht: require("./../../img/4khesht.png"),
    _5khesht: require("./../../img/5khesht.png"),
    _6khesht: require("./../../img/6khesht.png"),
    _7khesht: require("./../../img/7khesht.png"),
    _8khesht: require("./../../img/8khesht.png"),
    _9khesht: require("./../../img/9khesht.png"),
    _10khesht: require("./../../img/10khesht.png"),
    _11khesht: require("./../../img/11khesht.png"),
    _12khesht: require("./../../img/12khesht.png"),
    _13khesht: require("./../../img/13khesht.png"),
}
const Card = ({ card, style }) => {

    const img = "_" + card[0] + card[1];
    const image_source = images[img]
    return (
        <View style={style}>
            <Image
                style={styles.image}
                source={image_source}
            />
        </View>
    )

}
const styles = StyleSheet.create({
    image: {
        width: 100,
        height: 150,
    }
});
export default Card;